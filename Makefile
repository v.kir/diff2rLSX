CC=g++
CFLAGS= -std=c++14 -O2 -Wall
LDFLAGS=-static
SOURCES=test.cpp ciphers/*.cpp common/*.cpp
HEADERS=mds_2round_dyn_prog_est.h mds_2round_est_basic.h
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=lsx2r_medp_melp

all: $(SOURCES) $(HEADERS) $(EXECUTABLE)

$(EXECUTABLE): $(SOURCES) $(HEADERS)
	$(CC) $(CFLAGS) $(SOURCES) -o $(EXECUTABLE)

clean:
	rm $(EXECUTABLE)
