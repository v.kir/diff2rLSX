/*
 * linear.h
 *
 */

#ifndef COMMON_LINEAR_H_
#define COMMON_LINEAR_H_

#include "../common/std_headers.h"
#include "../common/arrays.h"
#include "../common/bit_functions.h"
#include "../common/hamming_weight.h"
#include "../common/differential.h"

using LAT = Matrix<double>;

bool scalar_mul(size_t a, size_t b)
{
	return hw::compute_hw64(a&b) % 2;
}

template<typename Arr>
Matrix<size_t> compute_lat(const Arr &sbox)
{
	const size_t sbox_size = sbox.size();
	Matrix<size_t> lat(sbox_size, sbox_size);

	for (size_t a = 0; a < sbox_size; a++)
	{
		for (size_t b = 0; b < sbox_size; b++)
		{
			for (size_t x = 0; x < sbox_size; x++)
			{
				if (scalar_mul(a,x) == scalar_mul(b, sbox[x]))
					lat(a, b)++;
			}
		}
	}
	return lat;
}

template<typename Arr>
Matrix<size_t> compute_bias_lat(const Arr &sbox)
{
	const size_t sbox_size = sbox.size();
	auto lat = compute_lat(sbox);

	for (size_t a = 0; a < sbox_size; a++)
	{
		for (size_t b = 0; b < sbox_size; b++)
		{
			int dom = lat(a,b);
			dom = abs(dom - int(sbox_size/2));
			lat(a,b) = dom;
		}
	}
	return lat;
}

double lp_to_pr_bias(double lp)
{
	return sqrt(lp)/2;
}

size_t lp_to_freq_bias(double lp, size_t N)
{
	double pr_bias = lp_to_pr_bias(lp);
	return pr_bias*N;
}

template<typename Arr>
LAT compute_norm_lat(const Arr &sbox)
{
	const size_t sbox_size = sbox.size();
	LAT norm_lat(sbox_size, sbox_size);
	auto lat = compute_lat(sbox);

	//normed as in Keliher's article
	for (size_t a = 0; a < sbox_size; a++)
	{
		for (size_t b = 0; b < sbox_size; b++)
		{
			LAT::Type v = lat(a,b);
			LAT::Type delta = v/sbox_size - 0.5;
			norm_lat(a,b) = (2*delta) * (2*delta);
		}
	}

	return norm_lat;
}

template<typename T>
auto compute_max_in_out_lin(const vector<T> &sbox)
{
	auto lat = compute_norm_lat(sbox);
	return compute_max_table_pr(lat);
}

template<typename T>
auto compute_max_out_in_lin(const vector<T> &sbox)
{
	auto lat = compute_norm_lat(sbox);
	return compute_max_table_pr(lat, true);
}

template<typename T>
auto compute_max_lin_in_out_lists(const vector<T> &sbox)
{
	auto lat = compute_norm_lat(sbox);
	return compute_max_elem_lists(lat);
}


template<typename T>
auto compute_max_lin_out_in_lists(const vector<T> &sbox)
{
	auto lat = compute_norm_lat(sbox);
	return compute_max_elem_lists(lat, true);
}

template<typename T>
vector<vector<T>> create_possible_lin_list(const vector<T> &sbox, bool in_to_out = true, double min_val = 0)
{
	LAT lat = compute_norm_lat(sbox);
	vector<vector<T>> lists(sbox.size());
	for (size_t dx = 0; dx < sbox.size(); dx++)
	{
		for (size_t dy = 0; dy < sbox.size(); dy++)
		{
			if (lat(dx,dy) > min_val)
			{
				if (in_to_out)
					lists[dx].push_back(dy);
				else
					lists[dy].push_back(dx);
			}
		}
	}
	return lists;
}

vector<double> create_major_lat_sbox_distr(const vector<uint8_t> &sbox, bool inverse = false)
{
	auto lat = compute_norm_lat(sbox);

	if (inverse)
	{
		lat.transpose();
	}

	auto data = lat.get_data();

	for (auto &row: data)
		sort(row.begin(), row.end(), greater<double>());

	vector<double> major;
	for (size_t i = 0; i < sbox.size(); i++)
	{
		double max = 0;
		for (size_t j = 1; j < sbox.size(); j++)
			if (data[j][i] > max)
				max = data[j][i];
		if (max > 0)
			major.push_back(max);
		else
			break;
	}

	return major;
}

bool scalar_product(const vector<uint8_t> &x, const vector<uint8_t> &y)
{
	size_t h = 0;
	for (size_t i = 0; i < x.size(); i++)
		h += hw::compute_hw8(x[i] & y[i]);
	return h%2;
}



#endif /* COMMON_LINEAR_H_ */
