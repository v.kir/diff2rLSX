/*
 * print_functions.h
 *
 */

#ifndef COMMON_PRINT_FUNCTIONS_H_
#define COMMON_PRINT_FUNCTIONS_H_

#include "../common/std_headers.h"
#include "../common/bit_functions.h"

template<typename Arr>
string block_to_hex(const Arr &block, size_t block_size)
{
	stringstream out;

	out << hex << setfill('0');
	for (size_t i = 0; i < block_size; i++)
		out << setw(2) << int(block[i]);

	return out.str();
}

template<typename T>
string block_to_hex(const vector<T> &block)
{
	return block_to_hex(block, block.size());
}

string vec_to_str(const vector<double> &vec);

template<typename Int>
string vec_to_str(const vector<Int> &vec)
{
	stringstream out;
	for (auto e: vec)
		out << int(e) << " ";
	return out.str();
}


template<typename Int>
string vec_to_str(const vector<vector<Int>> &vec2d)
{
	stringstream out;

	for (auto &vec: vec2d)
	{
		for (auto e: vec)
		{
			out << int(e) << " ";
		}
		out << endl;
	}

	return out.str();
}

template<typename T>
string to_bin(T x, const size_t bit_count = sizeof(T)*8)
{
	stringstream out;
	for (bool b: uint_to_bin(x, bit_count))
	{
		out << b;
	}
	return out.str();
}

template<typename Arr>
string block_to_bin(const Arr &block, size_t block_size)
{
	stringstream out;

	for (size_t i = 0; i < block_size; i++)
		out << to_bin(block[i]);

	return out.str();
}

template<typename T>
string block_to_bin(const vector<T> &block)
{
	return block_to_bin(block, block.size());
}

template<typename T>
string to_hex(T x, const size_t byte_count = sizeof(T))
{
	stringstream out;
	out << hex << setfill('0') << setw(byte_count*2) << x;
	return out.str();
}

string arr_to_bin(uint64_t *block, size_t block_count = 4);

#endif /* COMMON_PRINT_FUNCTIONS_H_ */
