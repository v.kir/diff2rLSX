/*
 * print_functions.cpp
 *
 */


#include "print_functions.h"

string arr_to_bin(uint64_t *block, size_t block_count)
{
	stringstream out;
	for (size_t j = 0; j < block_count; j++)
	{
		out << to_bin(block[j]);
		out << " ";
	}
	return out.str();
}

string vec_to_str(const vector<double> &vec)
{
	stringstream out;
	for (auto e: vec)
		out << e << " ";
	return out.str();
}
