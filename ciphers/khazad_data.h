/*
 * khazad_data.h
 *
 */

#ifndef KHAZAD_KHAZAD_DATA_H_
#define KHAZAD_KHAZAD_DATA_H_

#include "../common/std_headers.h"
#include "../common/arrays.h"

class KhazadData
{
	public:
		static const size_t sbox_size = 256;
		static const size_t sbox_bit_size = 8;
		static const size_t block_byte_size = 8;
		static const size_t code_distance = 9;
		static const size_t mds_matrix_size = 8;
		static const size_t irred_pol_coef = 0b100011101; //x^8 + x^4 + x^3 + x^2 + 1
		static const uint8_t sbox[sbox_size];


		static vector<uint8_t> get_sbox();
		static Matrix<uint8_t> get_mds_matrix(bool inv = false);
};


#endif /* KHAZAD_KHAZAD_DATA_H_ */
